#use an existing docker image as a base
FROM node:alpine as builder

# Switch Workdir to /usr/app
WORKDIR '/app'

copy package.json .

# Download and install a dependencey
RUN npm install

# Copy our current directory into the container 
COPY . .

Run npm run build

FROM nginx
EXPOSE 80
COPY --from=builder /app/build /usr/share/nginx/html


# Tell the image what to do when it starts
# as  a container
